# Monitoring Stack

Examples for self-hosting [prometheus](https://prometheus.io), [loki](https://grafana.com/oss/loki/), and [grafana](https://grafana.com/) for monitoring the [Safe Surfer Core platform for ISPs & orgs](https://safesurfer.io/for-isp).

## Installing
Check the readme inside the `prometheus`, `loki`, and `grafana` folders (in that order). The Safe Surfer Core helm chart includes dashboards for monitoring the deployments, but requires these tools to be installed to collect and view data. These are just example instructions for setting up the monitoring stack, you can also configure the Safe Surfer Core chart to work with your existing stack.
